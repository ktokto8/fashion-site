<?php

// PDO SQL Adapter. Handles SQL queries
class sqlHandler
{
    private $conn;
    
    // Connect to database
    public function __construct($serverName, $DBName, $username, $password)
    {
        try 
        {
            $this->conn = new PDO("mysql:host=$serverName;dbname=$DBName", $username, $password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo "Connection establishment failed: " . $e->getMessage();
        }
    }
    
    // Post Query
    public function postQuery($sql){
        try{
            $this->conn->exec($sql);
        }catch(PDOException $e){
            echo "Query failed: " . $e->getMessage();
        }
    }
    
    public function getData($sql){
        try{
            $data = $this->conn->prepare($sql);
            $data->execute();
            
            $result = $data->setFetchMode(PDO::FETCH_ASSOC);
            return $data->fetchAll();
        }catch(PDOException $e){
            echo "Query failed: " . $e->getMessage();
        }
    }
    
    public function execute($sql){
        try{
            $data = $this->conn->prepare($sql);
            $data->execute();
            
            //$result = $data->setFetchMode(PDO::FETCH_ASSOC);
            return $data->fetch();
        }catch(PDOException $e){
            echo "Query failed: " . $e->getMessage();
        }
    }
    
    // Disconnect from database
    public function disconnect(){
        $this->conn = NULL;
    }
    
    public function __destruct(){
        $this->conn = NULL;
    }
}

?>