<?php

// includes. SQL adapter
require_once('phpLib/sqlAdapter.php');

// definitions


class ImageLoader
{
    private $target_dir, $target_file, $uploadOk, $imageFileType, $fileName;
    
    public function __construct($file, $serverImageDir)
    {
        if(empty($_FILES[$file]["tmp_name"])){
            $this->uploadOk = 0;
            return;
        }else{
            $this->uploadOk = 1;
        }
        
        $this->fileName = $file;
        $this->target_dir = $serverImageDir;
        $this->target_file = $this->target_dir . $_FILES[$file]["name"];
        $this->imageFileType = pathinfo($this->target_file, PATHINFO_EXTENSION);
    }
    
    public function FileCheck()
    {
        $check = getimagesize($_FILES[$this->fileName]["tmp_name"]);
        if($check !== false) {
            $this->uploadOk = 1;
        } else {
            $this->uploadOk = 0;
        }

        // Check file size
        if ($_FILES[$this->fileName]["size"] > 1000000) {
            $this->uploadOk = 0;
        }
        
        // Allow certain file formats
        if($this->imageFileType != "jpg" && $this->imageFileType != "png" && $this->imageFileType != "jpeg"
           && $this->imageFileType != "gif" ) {
                $this->uploadOk = 0;
        }
        return $this->uploadOk;
    }
    
    public function Upload()
    {
        if (!move_uploaded_file($_FILES[$this->fileName]["tmp_name"], $this->target_file)) {
            $this->uploadOk = 0;
        }
        return $this->uploadOk;
    }
           
    public function TargetFilepath(){
        return $this->target_file;
    }
    
    public function Status(){
        return $this->uploadOk;
    }
} // Loads image for preview and uploads it to server


function PostLoader($pageNr, $content)
{
    $postCount = 5;
    
    if($pageNr == 0){ // to not waste resources
        return -1; 
    }
    
    $base = new sqlHandler('localhost', 'FashionSite', 'root', '');
    $count = $base->execute("SELECT COUNT(*) FROM posts");
    
    
    $count = $content == -1 ? $base->execute("SELECT COUNT(*) FROM posts") :  $count = $base->execute("SELECT COUNT(*) FROM posts WHERE postCont = $content");  // optimisable

    if($count[0] == 0){
        return -1;
    }
    
    $pgcount = ceil($count[0] / $postCount);
    
    if($pageNr > $pgcount){
        return -1;     // prev pressed when current page is 1, or nex pressed when current page is last
    }
    
    $lim = $postCount;
    $ofs = ($pageNr - 1) * $postCount;
    $result = $content == -1 ? $base->getData("SELECT * FROM posts ORDER BY postID DESC LIMIT $lim OFFSET $ofs") : $base->getData("SELECT * FROM posts WHERE postCont = $content ORDER BY postID DESC LIMIT $lim OFFSET $ofs");
    
    foreach($result as $v){
        LoadPost($v);
    }
    
    echo('<div class="pagimation">
            <div class="pagimation-elms">
                <a class="pgPrev">&#10094;</a>');
    
    $first = 1;
    if($pgcount > 5){
        if($pageNr > 3 && $pageNr <= $pgcount - 3){
            $first = $pageNr - 2;
        }else if($pageNr > $pgcount - 3){
            $first = $pgcount - 4;
        }
    }
    
    $last = $first + 4;
    $last = ($last <= $pgcount) ? $last : $pgcount;
    
    for($i = $first; $i <= $last; $i++){
        if($i != $pageNr){
            echo('<a class="pagimation-btn" href="index.php?pgi=' . $i . '&cont=' . $content . '">' . $i . '</a>');
        }else{
            echo('<a class="pagimation-btn activePg" href="index.php?pgi=' . $i . '&cont=' . $content . '">' . $i . '</a>');           
        }
    }   
    
    echo('<a class="pgNext"');
    if($pageNr < $pgcount){
        echo(' href="index.php?pgi=' . ($pageNr + 1) . '&cont=' . $content . '"');
    }
    echo('>&#10095;</a></div></div>');
}

function LoadPost($post){
    switch($post['postType']){
        case 1:
            echo('<div class="row">
                        <div class="col-12">
                            <div class="blextpost">
                                <img class="blextpost-img" src="' . $post['postImg'] . '"/>
                                <div class="blextpost-text">
                                    <p class="postpar">' . $post['postText'] . '</p>	
                                </div>
                            </div>
                        </div>
                    </div>');
            break;
            
        case 2: 
            echo('<div class="row">
                    <div class="col-left-9">
                        <div class="post">
                            <div class="post-text">
                                <p>' . $post['postText'] . '</p>	
                            </div>
                            <img class="post-img" src="' . $post['postImg'] . '"/>
                        </div>
                    </div>
                </div>'
            );
            break;
            
        case 3: 
            echo('<div class="row">
                    <div class="col-right-9">
                        <div class="post">
                            <div class="post-text">
                                <p>' . $post['postText'] . '</p>	
                            </div>
                            <img class="post-img" src="' . $post['postImg'] . '"/>
                        </div>
                    </div>
                </div>'
            );
            break;
    }
}

function SlideLoader($content)
{
    $base = new sqlHandler('localhost', 'FashionSite', 'root', '');
    
    
    
    if($content == -1){ // all types
        $slcount = $base->execute("SELECT COUNT(*) FROM posts WHERE featured = 1");  // optimisable
        if($slcount[0] == 0){
            return -1;
        }
        $slides = $base->getData("SELECT * FROM posts WHERE featured = 1 ORDER BY postID DESC");
    }
    else{ // one type
        $slcount = $base->execute("SELECT COUNT(*) FROM posts WHERE featured = 1 AND postCont = $content");  // optimisable
        if($slcount[0] == 0){
            return -1;
        }
        $slides = $base->getData("SELECT * FROM posts WHERE featured = 1 AND postCont = $content ORDER BY postID DESC");
    }
    
    
    
    
    echo('<div class="row">
            <div class="col-12">
                <div class="slide-container">');

    foreach($slides as $v){
        echo('<div class="slide">
                <div class="slide-text">
                    <p>' . $v['postName'] . '</p>
                </div>
                <img class="slide-img" src="' . $v['postImg'] . '"/>
            </div>');
    }


    echo('  </div>
                <div class="slide-bar">
                    <div class="slide-bar-elms">
                        <a class="prev">&#10094;</a>');

    for($i = 0; $i < $slcount[0]; $i++){
        echo('<span class="dot"></span>');
    }

    echo('              <a class="next">&#10095;</a>
                    </div>
                </div>
            </div>
        </div>');
}


?>