<?php
    require_once('loader.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>FashionSiteAdmin</title>

    <!-- Reset tool -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css"/>

    <link rel="stylesheet" type="text/css" href="css/cssframework.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/poster.css"/>

    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <div class="nav">
        <img class="menuicon" src="images/menu.svg"/>
        <div class="navbuttons">
            <a class="navbtn" id="navf">Fashion</a>
            <a class="navbtn" id="navn">Nature</a>
            <a class="navbtn">So Fancy</a>
            <a class="navbtn">Wow</a>
        </div>
    </div>
    
    <div class="container">

        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img class="center-block" src="images/logo.svg" alt="logo"/>
                </div>
            </div>
        </div>

        <div class="posts">
            <!-- new possible posts. Hidden on load -->
            <div class="row" id="newPostType1" hidden>
                <div class="col-12">
                    <div class="blextpost">
                        <img class="blextpost-img" src=""/>
                        <div class="blextpost-text">
                            <p class="postpar"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="newPostType2" hidden>
                <div class="col-left-9">
                    <div class="post">
                        <div class="post-text">
                            <p></p>	
                        </div>
                        <img class="post-img" src=""/>
                    </div>
                </div>
            </div>

            <div class="row" id="newPostType3" hidden>
                <div class="col-right-9">
                    <div class="post">
                        <div class="post-text">
                            <p></p>	
                        </div>
                        <img class="post-img" src=""/>
                    </div>
                </div>
            </div>
           

            <div style="position: absolute; left: 20%;">
                <br/><br/>
                <textarea form="postform" name="posttext" cols="100" rows="10"></textarea>
                <form id="postform" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                    <br/><br/>
                        <input type="file" name="postimage" id="postimage"/><br/><br/>
                        <b>Post type:</b><br/>

                        Type 1: <input type="radio" name="posttype" value="1" checked="true"/> (text below)<br/>
                        Type 2: <input type="radio" name="posttype" value="2" /> (text on, left align)<br/>
                        Type 3: <input type="radio" name="posttype" value="3" /> (text on, right align)<br/>
                        <br/><br/><b>
                        Post content:</b><br/>
                        Type 1: <input type="radio" name="postcont" value="1" checked="true"/> (fashion)<br/>
                        Type 2: <input type="radio" name="postcont" value="2" /> (nature)<br/><br/><br/>

                    <input type="submit" name="upload" value="Upload"/>
                </form>    
                <br/>
                <br/>
            </div>


            <?php
                // definitions
                define('LOCAL_IMAGE_DIR', 'images/');
                define('SERVER_IMAGE_DIR', 'images/');

                if(isset($_POST['upload'])){
                    // Upload image to server
                    $target_file = '';
                    $text = '';
                    $loader = NULL;

                    // Load file to temp
                    $loader = new ImageLoader('postimage', LOCAL_IMAGE_DIR);

                    // Check if file was successfully loaded to temp folder
                    if($loader->Status()) 
                    {
                        $loader->FileCheck(); // Check if filetype is campatible and that file is under size limit
                        $loader->Upload(); // Upload file to server

                        // Check if text exists
                        if(empty($_POST['posttext']))
                        {
                            echo "Text missing!";
                        }
                    } else {
                        echo "Image missing!";
                    }

                    // Upload post data (text, content type, image path, date, post type)
                    $servername = "localhost";
                    $database = "FashionSite";
                    $username = "root";
                    $password = "";

                    $conn = new sqlHandler($servername, $database, $username, $password);

                    // All data was aquired from last submission
                    $postTxt = $_POST['posttext'];
                    $imgPath = SERVER_IMAGE_DIR . $_FILES['postimage']["name"];
                    $postType = $_POST['posttype'];
                    $cont = $_POST['postcont'];

                    $conn->postQuery("INSERT INTO posts (postType, postImg, postText, postDate, postCont)
                                VALUES($postType, '$imgPath', '$postTxt', CURDATE(), $cont)");
                    echo '<div style="text-align:center" id="postInserted">Post inserted.</div><br/>';  // Shows that post was successfully inserted
                }
            ?>
        
        
        </div>
    </div>


    <div class="footer">
        Ugnė Avižinytė 2016&copy
    </div>
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- my scripts -->
    <script src="javasc/clientscript.js"></script>
    <!-- New post real time view script -->
    <script src="javasc/admin.js"></script>
</body>
</html>