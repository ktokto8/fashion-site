<!DOCTYPE html>
<html lang="en">
<head>
    <title>FashionSite</title>

    <!-- Reset tool -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css"/>

    <link rel="stylesheet" type="text/css" href="css/cssframework.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>

    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <div class="nav">
        <img class="menuicon" src="images/menu.svg"/>
        <div class="navbuttons">
            <a class="navbtn" href="index.php?pgi=1&cont=1">Fashion</a>
            <a class="navbtn" href="index.php?pgi=1&cont=2">Nature</a>
            <a class="navbtn">So Fancy</a>
            <a class="navbtn">Wow</a>
        </div>
    </div>


    <div class="container">

        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img class="center-block" src="images/logo.svg" alt="logo"/>
                </div>
            </div>
        </div>

        <div class="posts">

            <?php 
                require_once('loader.php');
            
                if(isset($_REQUEST['pgi']) && isset($_REQUEST['cont'])){
                    $cont = $_REQUEST['cont'];  // What content to load
                    $pgi = $_REQUEST['pgi'];    // Which page to load
                    
                    SlideLoader($cont);
                    PostLoader($pgi, $cont);   
                }else{
                    SlideLoader(-1);
                    PostLoader(1, -1);
                }
            ?>
        
        </div>
    </div>


    <div class="footer">
        Ugnė Avižinytė 2017&copy
    </div>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- my scripts -->
    <script src="javasc/clientscript.js"></script>
</body>
</html>