
var postType = '';  
var postContent = '';
var reader = null;
var postImage = null;

// Show new post of appropriate type
function showPost(){
    $('#postInserted').remove();

    postType = $('input[name=posttype]:checked').val();
    postContent = $('input[name=postcont]:checked').val();

    $('.newPost').hide();
    $('.newPost').removeClass('newPost');

    switch(postType){
        case '1':
            // window.alert(image);
            $('#newPostType1').addClass('newPost');
            $('#newPostType1').show();
            $('#newPostType1 p').html($('textarea[name=posttext]').val());
            $('#newPostType1 img').attr('src', postImage);
            break;
        case '2':
            $('#newPostType2').addClass('newPost');
            $('#newPostType2').show();
            $('#newPostType2 p').html($('textarea[name=posttext]').val());
            $('#newPostType2 img').attr('src', postImage);
            break;
        case '3':
            $('#newPostType3').addClass('newPost');
            $('#newPostType3').show();
            $('#newPostType3 p').html($('textarea[name=posttext]').val());
            $('#newPostType3 img').attr('src', postImage);
            break;
    }
}

// Shows new post when image is loaded
$('#postimage').change(function(){

    if (this.files && this.files[0]) {
        reader = new FileReader();

        reader.onload = function (e) {
            postImage = e.target.result;

            showPost();
        }


        reader.readAsDataURL(this.files[0]);
    }
});

// Updates new post's text when you type in textarea in real time
$('textarea[name=posttext]').keyup(function(){
    switch(postType){
        case '1':
            $('#newPostType1 p').html($('textarea[name=posttext]').val().replace(/(?:\r\n|\r|\n)/g, '<br/>'));
            break;
        case '2':
            $('#newPostType2 p').html($('textarea[name=posttext]').val().replace(/(?:\r\n|\r|\n)/g, '<br/>'));
            break;
        case '3':
            $('#newPostType3 p').html($('textarea[name=posttext]').val().replace(/(?:\r\n|\r|\n)/g, '<br/>'));
            break;
    }
});

// Changes post appearance, when type is changed
$('input[name=posttype]').change(function(){
    if(postImage){
        showPost();
    }
});
