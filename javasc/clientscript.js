// <navigation panel>
var navCollapse = function(){
    if($('.nav').hasClass('active')){
        $('.nav').animate({right: '-152pt'});
        $('.nav').removeClass('active');
    }
};

var navigationAction = function(){
	$('.nav').removeClass('active');
	$('.menuicon').click(function () {   
		if($('.nav').hasClass('active')){
			$('.nav').animate({right: '-152pt'});
			$('.nav').removeClass('active');
		}else{
			$('.nav').animate({right: '0pt'});
			$('.nav').addClass('active');
		}
	});
    
};
// </navigation panel>

var slideShowAction = function () {
	var dotCount = $('.dot').last().index() - 1;
	var actDot = 0;

	$('.dot').eq(actDot).css('opacity', '1.0');

	var slideCount = $('.slide-container').children().length;
	var actSlide = 0;

	$('.slide').hide();
	var slideArray = $('.slide-container').children();
	slideArray.eq(actSlide).show();
	slideArray.eq(actSlide).children('.slide-text').hide();
	

	$('.prev').click(function () {
		var actSlidePrev = actSlide;
		if(actSlide !== 0){
			actSlide--;
			slideArray.eq(actSlidePrev).fadeOut(100, function(){
				slideArray.eq(actSlide).fadeIn();
				slideArray.eq(actSlide).children('.slide-text').hide();
			});
		}else{
			actSlide = slideCount - 1;
			slideArray.eq(actSlidePrev).fadeOut(100, function(){
				slideArray.eq(actSlide).fadeIn();
				slideArray.eq(actSlide).children('.slide-text').hide();
			});
		}
		$('.dot').eq(actDot).animate({opacity: '0.5'}, 100);
		actDot = actSlide;
		$('.dot').eq(actDot).animate({opacity: '1.0'}, 100);
	});

	$('.next').click(function () {
		var actSlidePrev = actSlide;
		if(actSlide != slideCount - 1){
			actSlide++;
			slideArray.eq(actSlidePrev).fadeOut(100, function () {
				slideArray.eq(actSlide).fadeIn();
				slideArray.eq(actSlide).children('.slide-text').hide();
			});
		}else{
			actSlide = 0;
			slideArray.eq(actSlidePrev).fadeOut(100, function () {
				slideArray.eq(actSlide).fadeIn();
				slideArray.eq(actSlide).children('.slide-text').hide();
			});
		}
		$('.dot').eq(actDot).animate({opacity: '0.5'}, 100);
		actDot = actSlide;
		$('.dot').eq(actDot).animate({opacity: '1.0'}, 100);
	});

	$('.slide').hover(
		function () {
            var slide = $(this);
            if(!slide.hasClass('active')){
                slide.addClass('active');
                slide.children('.slide-img').animate({opacity: '0.2'}, 300);
                slide.children('.slide-text').fadeIn(300);
            }
		},
		function () {
            var slide = $(this);
            if(slide.hasClass('active') && !slide.hasClass('fadeout')){
                slide.addClass('fadeout');
                slide.children('.slide-img').animate({opacity: '1.0'}, 300, function(){
                    slide.removeClass('fadeout');
                    slide.removeClass('active');
                });
                slide.children('.slide-text').fadeOut(300);
            }
		}
        
	);

	$('.dot').hover(
		function () {
            var dot = $(this);
            if(!dot.hasClass('active')){
                dot.addClass('active');
                $(this).animate({opacity: '1.0'}, 100);
            }
		},
		function () {
			if($(this).index() - 1 != actDot){
                var dot = $(this);
                if(dot.hasClass('active') && !dot.hasClass('fadeout')){
                    dot.addClass('fadeout');
                    dot.animate({opacity: '0.5'}, 100, function(){
                        dot.removeClass('active');
                        dot.removeClass('fadeout');
                    });
                }
            }
		}
	);

	$('.dot').click(function () {
		var dot = $(this);
		if(actSlide != dot.index() - 1){
			slideArray.eq(actSlide).fadeOut(100, function () {
				actSlide = dot.index() - 1;
				slideArray.eq(actSlide).fadeIn();
				slideArray.eq(actSlide).children('.slide-text').hide();

				$('.dot').eq(actDot).animate({opacity: '0.5'}, 100);
				actDot = actSlide;
				$('.dot').eq(actDot).animate({opacity: '1.0'}, 100);
			});
		}
	});                            // </slide show>
};   // slide show
var showcasePostAction = function () {
	$('.post-text').hide();
	$('.post').hover(
		function () {
            var actpost = $(this);
            if(!actpost.hasClass('active')){
                actpost.addClass('active');
                actpost.children('.post-img').animate({opacity: '0.2'}, 300);
                actpost.children('.post-text').fadeIn(300);
            }
		},
		function () {
            var actpost = $(this);
            if(actpost.hasClass('active') && !actpost.hasClass('fadeout')){
                actpost.addClass('fadeout');
                actpost.children('.post-img').animate({opacity: '1.0'}, 300, function(){
                    actpost.removeClass('fadeout');
                    actpost.removeClass('active');
                });
                actpost.children('.post-text').fadeOut(300);
            }
		}
	);
};   // showcase post

var pagimation = function(){
    $('.pagimation-btn').click(function(){
        var pgmBtn = $(this);
        if(!pgmBtn.hasClass('activePg')){
            var xmlhttp = new 
            XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    $('.posts').html(this.responseText);
                    reinit();
                }
            };
            var pgi = Number(window.sessionStorage.getItem("page_i"));
            pgi += pgmBtn.index() - $('.activePg').index();
            window.sessionStorage.setItem("page_i", pgi.toString());
            var cont = Number(window.sessionStorage.getItem("cont"));
            xmlhttp.open("GET", "populate.php?pgi=" + pgi + "&cont=" + cont, true);
            xmlhttp.send();
        }
    });
    
    $('.pgNext').click(function(){
        var pgi = Number(window.sessionStorage.getItem("page_i")) + 1;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if(this.responseText !== ""){
                    $('.posts').html(this.responseText);
                    window.sessionStorage.setItem("page_i", pgi.toString());
                    reinit();
                }
            }
        };
        var cont = Number(window.sessionStorage.getItem("cont"));
        xmlhttp.open("GET", "populate.php?pgi=" + pgi + "&cont=" + cont, true);
        xmlhttp.send();
    });
    
    $('.pgPrev').click(function(){
        var pgi = Number(window.sessionStorage.getItem("page_i")) - 1;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if(this.responseText !== ""){
                    $('.posts').html(this.responseText);
                    window.sessionStorage.setItem("page_i", pgi.toString());
                    reinit();
                }
            }
        };
        var cont = Number(window.sessionStorage.getItem("cont"));
        xmlhttp.open("GET", "populate.php?pgi=" + pgi + "&cont=" + cont, true);
        xmlhttp.send();
    });
};

function reinit(){
    showcasePostAction();
    pagimation();
    navCollapse();
    slideShowAction();
    window.scrollTo(0, 0);
}

/*
function Prescroll(){
    var data = document.cookie; // copy cookies
    var titleLength = document.title.length; // find page title length
    var pos = data.indexOf(document.title); // find position of title
    var left = data.slice(pos + titleLength + 1); // slice up to scroll value
    var param = left.split(";");    // cut scroll value
    window.scrollTo(0, param[0]);   // scroll
}

// execute
Prescroll();
*/
$(document).ready(function(){
    window.sessionStorage.setItem("page_i", "1");
    window.sessionStorage.setItem("cont", "-1");
    
    window.sessionStorage.setItem("mainY", "0");
    window.sessionStorage.setItem("fashionY", "0");
    window.sessionStorage.setItem("natureY", "0");
    

    
    navigationAction();
    
    slideShowAction();
    
    showcasePostAction();
    
    //pagimation();

    
    /*$(document).click(function(){
        //document.cookie = document.title + "=" + window.pageYOffset;
    });*/
    

});
       